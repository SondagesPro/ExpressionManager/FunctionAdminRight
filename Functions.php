<?php

/**
 * This file is part of FunctionAdminRight plugin
 * @version 0.1.0
 */

namespace FunctionAdminRight;

use Yii;
use LimeExpressionManager;
use Permission;

class Functions
{
    /**
     * Return right on survey
     * By default return permssion to update reponse
     * @param string $permission : any equation
     * @param string $crud : any equation
     * @return boolean for Expression manager (1 or '').
     */
    public static function haveAdminRight($permission = 'responses', $crud = 'update')
    {
        if (Yii::app() instanceof CConsoleApplication) {
            return 1;
        }
        if (Yii::app()->user->isGuest) {
            return '';
        }
        $surveyId = LimeExpressionManager::getLEMsurveyId();
        if (Permission::model()->hasSurveyPermission($surveyId, $permission, $crud)) {
            return 1;
        }
        return '';
    }
}
