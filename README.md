# FunctionAdminRight plugin for LimeSurvey

Expression Script: allow to get the right of current user on current survey. Return always false if user are not logged in admin GUI.

## Usage

The plugin can be used any expression, it return the right (permission) on current survey. By default return if user have right to update response.

You can show some question to administrator only with token survey with allow edition, or with [reloadAnyResponse](https://gitlab.com/SondagesPro/coreAndTools/reloadAnyResponse) plugin for example.

The function get 2 optionnal parameters

- `permission` see [avaible permission for survey](https://github.com/LimeSurvey/LimeSurvey/blob/77f5e93ad82e05824009841ef88b5873bc765314/application/models/Survey.php#L2144), by default `response`
- `crud` can bee 'read', 'create', 'update', 'delete', 'import', 'export'; default to `update`
