<?php

/**
 * @copyright 2022 Denis Chenu / SondagesPro
 * @author Denis Chenu <denis@sondages.pro>
 * @license GPL version 3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
class FunctionAdminRight extends PluginBase
{
    protected static $description = 'Return admin right on current survey.';
    protected static $name = 'FunctionAdminRight';

    /** @inheritdoc, this plugin didn't have any public method */
    public $allowedPublicMethods = array();

    public function init()
    {
        $this->subscribe('ExpressionManagerStart', 'newValidFunctions');
    }

    /**
     * @see https://manual.limesurvey.org/ExpressionManagerStart ExpressionManagerStart event
     * add the getAnswerOptionText static function to Expression Manager function
     * @return void
     */
    public function newValidFunctions()
    {
        Yii::setPathOfAlias("FunctionAdminRight", dirname(__FILE__));
        $newFunctions = array(
            'haveAdminRight' => array(
                '\FunctionAdminRight\Functions::haveAdminRight',
                null, // No javascript function : set as static function
                $this->gT("Return current permsision of user actually connected."), // Description for admin
                'string haveAdminRight([$permission = \'responses\' [, $crud = \'update\']])', // Extra description
                'https://limesurvey.org', // Help url
                0, 1, 2
            ),
        );
        $this->getEvent()->append('functions', $newFunctions);
    }
}
